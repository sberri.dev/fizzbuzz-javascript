let i = 0 //je déclare ma variable i 

for (let i = 0; i <= 100; i++) {// ici j'ai définit une boucle pour l'incrémenter

    let output = "";

    if (i % 3 === 0) { // j'ai définit des conditions pour vérifier si la valeur actuelle est un multiple de 3, 5, ou les deux, et afficher "Fizz", "Buzz" ou "FizzBuzz"

        output += "Fizz";
    }

    if (i % 5 === 0) {

        output += "Buzz";
    }

    if (output === "") {

        output = i;
    }

    console.log(output)

}


